import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarAdminComponent } from './admin/navbar/navbar.component';
import { SidebarComponent } from './admin/sidebar/sidebar.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { RouterModule } from "@angular/router";
import { BreadcrumbAdminComponent } from './admin/breadcrumb/breadcrumb.component';
import { TranslationComponent } from './admin/translation/translation.component';
import { NavbarPublicComponent } from './public/navbar/navbar.component';
import { FooterComponent } from "./public/footer/footer.component";
import { BreadcrumbPublicComponent } from "./public/breadcrumb/breadcrumb.component";
import { TranslateModule } from "@ngx-translate/core";
import { TranslationPublicComponent } from "./public/translation/translation.component";



@NgModule({
    declarations: [
        NavbarAdminComponent,
        SidebarComponent,
        BreadcrumbAdminComponent,
        TranslationComponent,
        NavbarPublicComponent,
        FooterComponent,
        BreadcrumbPublicComponent,
        TranslationPublicComponent
    ],
    exports: [
        SidebarComponent,
        NavbarAdminComponent,
        BreadcrumbAdminComponent,
        NavbarPublicComponent,
        FooterComponent,
        BreadcrumbPublicComponent,
        TranslationPublicComponent
    ],
    imports: [
        CommonModule,
        FontAwesomeModule,
        RouterModule,
        TranslateModule
    ]
})
export class ComponentsModule { }
