import { Component, OnInit } from '@angular/core';
import { faBars, faTimes } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-navbar-public',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarPublicComponent implements OnInit {

  faIcon = faBars;
  open = false;

  constructor() { }

  ngOnInit(): void {
  }

  toogleMenu(){
    if (this.open){
      this.open = false;
      this.faIcon = faBars;
    } else {
      this.open = true;
      this.faIcon = faTimes;
    }
  }

}
