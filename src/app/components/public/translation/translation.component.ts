import { Component, OnInit } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { TranslationService } from "../../../services/admin/translation.service";

@Component({
  selector: 'app-translation-public',
  templateUrl: './translation.component.html',
  styleUrls: ['./translation.component.sass']
})
export class TranslationPublicComponent implements OnInit {


  activeLang = localStorage.getItem('language');


  constructor(private translate: TranslateService,
              public service: TranslationService) {
    if (typeof this.activeLang === 'string') {
      this.translate.setDefaultLang(this.activeLang);
    }
  }

  ngOnInit(): void {
  }

  public changeLenguage(lang: string){
    this.activeLang = lang;
    this.translate.use(lang);
    localStorage.setItem('language', lang);
    this.service.setLang(lang);
  }

}
