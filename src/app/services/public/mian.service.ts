import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MianService {

  baseUrl = environment.baseUrl + '/api/home';

  constructor(private http: HttpClient) {

  }

  getData(){
    return this.http.get<any>(this.baseUrl);
  }

}
