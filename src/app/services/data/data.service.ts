import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  baseUrl = environment.baseUrl + '/api';
  access_token = <string>localStorage.getItem('access_token')
  headers:any;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  getDataAdmin(){
    return this.http.get<any>(this.baseUrl + '/data/admin', {headers: this.headers});
  }

  getDataPublic(){
    return this.http.get<any>(this.baseUrl + '/data/public', {headers: this.headers});
  }
}
