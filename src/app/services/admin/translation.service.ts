import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  public language = localStorage.getItem('language');

  constructor() { }

  setLang(lang: string){
    this.language = lang;
    localStorage.setItem('language', lang);
  }

  getLang(){
    return this.language;
  }
}
