import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GalleriesService {

  url = environment.baseUrl + '/api/galleries';
  url_im = environment.baseUrl;
  access_token = <string>localStorage.getItem('access_token');
  headers:any;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  getGalleries(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }


  showGallery(id: number){
    return this.http.get<any>(this.url + '/' + id, {headers: this.headers});
  }

  activeGalleries(id: number){
    return this.http.get<any>(this.url + '/' + id + '/edit', {headers: this.headers});
  }

  postGalleries(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putGalleries(id: any, params: any){
    return this.http.post<any>(this.url + '/update/' + id, params, {headers: this.headers});
  }

  deleteGalleries(id: number){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }

  deleteImage(id: number){
    return this.http.delete<any>(this.url_im + '/' + id, {headers: this.headers});
  }

  activeImage(id: number){
    return this.http.get<any>(this.url_im + '/' + id + '/edit', {headers: this.headers});
  }
}
