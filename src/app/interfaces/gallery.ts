export interface Gallery {
  id: number;
  name: {
    es: string;
    fr: string;
    de: string;
  };
  slug: {
    es: string;
    fr: string;
    de: string;
  };
  type: string;
  reference: number;
  active: boolean
}
