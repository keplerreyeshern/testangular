import { Component, OnInit } from '@angular/core';
import { faList, faWarehouse, faSave, faEye, faEyeSlash, faImages, faHandPointer, faPowerOff, faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons';
import { Alert } from "../../../../interfaces/alert";
import { TranslationService } from "../../../../services/admin/translation.service";
import { CategoriesService } from "../../../../services/admin/categories.service";
import { NgxSpinnerService } from "ngx-spinner";
import {ActivatedRoute, Router} from "@angular/router";
import { NgForm } from "@angular/forms";
// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {Title} from "@angular/platform-browser";
import {User} from "../../../../interfaces/user";
import {Category} from "../../../../interfaces/category";
import {GalleriesService} from "../../../../services/admin/galleries.service";
import {Image} from "../../../../interfaces/image";
import {environment} from "../../../../../environments/environment";
import {Gallery} from "../../../../interfaces/gallery";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faWarehouse = faWarehouse;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  faEdit = faEdit;
  language = localStorage.getItem('language');
  url_images = environment.baseUrl;
  public Editor = ClassicEditor;
  category: Category = <Category>{};
  alert: Alert = {
    type: 'danger',
    message: '',
    active: false
  };
  gallery: Gallery = <Gallery>{};
  image:any;
  imageInit:any;
  thumbnail:any;
  id:any;
  name:any;
  description:any;
  editImage = true;
  images: Image[] = [];
  files: File[] = [];

  constructor(private activatedRoute: ActivatedRoute,
              public serviceTranslation: TranslationService,
              private titleService: Title,
              private serviceImage: GalleriesService,
              private service: CategoriesService,
              private loading: NgxSpinnerService,
              private router: Router) {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['id']);
    });
  }

  getData(id: string){
    let categories = JSON.parse(<string>localStorage.getItem('categories_admin'));
    let images = JSON.parse(<string>localStorage.getItem('images_admin'));
    let galleries = JSON.parse(<string>localStorage.getItem('galleries_admin'));
    // @ts-ignore
    this.category = categories.find(item => item.id == id);
    // @ts-ignore
    let galleriesCate = galleries.filter(item => item.type == 'categories');
    // @ts-ignore
    this.gallery = galleriesCate.find(item => item.reference == this.category.id);
    // @ts-ignore
    this.images = images.filter(item => item.gallery_id == this.gallery.id);
    if (this.serviceTranslation.getLang() == 'fr'){
      this.titleService.setTitle("Editar " + this.category.name.fr);
    } else if (this.serviceTranslation.getLang() == 'de'){
      this.titleService.setTitle("Editar " + this.category.name.de);
    }
    this.imageInit = this.category.image;
    // this.images = response.images;
    // this.gallery = response.gallery;
  }

  ngOnInit(): void {
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    // @ts-ignore
    let language = this.language.toString();
    let filesLength = this.files.length.toString();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('description', form.value.description);
    if (this.image){
      params.append('image', this.image);
    }
    params.append('language', language);
    params.append('fileslength', filesLength);
    for (let f = 0; f < this.files.length; f++){
      params.append('file'+f, this.files[f]);
    }
    this.service.putCategories(this.category.id, params).subscribe( response => {
      let category = response;
      let categories = JSON.parse(<string>localStorage.getItem('categories_admin'));
      // @ts-ignore
      const index = categories.findIndex(item => item.id == product.id);
      categories[index] = category;
      localStorage.setItem('categories_admin', JSON.stringify(categories));
      this.loading.hide();
      console.log(response);
      this.router.navigateByUrl('/admin/categories');
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  closed() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closed()
    },10000);
  }

  deleteImage(gallery: number){
    this.loading.show();
    this.serviceImage.deleteImage(gallery).subscribe( response => {
      // @ts-ignore
      const index = this.images.findIndex(item => item.id == response.id);
      this.images.splice(index, 1);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
    });
  }

  activeImage(gallery: number){
    this.loading.show();
    this.serviceImage.activeImage(gallery).subscribe( response => {
      const user = response;
      // @ts-ignore
      const index = this.images.findIndex(item => item.id == user.id);
      this.images[index].active = response.active;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
    });
  }

}
