import { Component, OnInit } from '@angular/core';
import { faList, faWarehouse, faSave, faEye, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { NgForm } from '@angular/forms';
// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { TranslationService } from '../../../../services/admin/translation.service';
import { CategoriesService } from '../../../../services/admin/categories.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { Alert } from "../../../../interfaces/alert";
import { ParentCategory } from "../../../../interfaces/parent-category";

@Component({
  selector: 'app-create-subcategories',
  templateUrl: './create-subcategories.component.html',
  styleUrls: ['./create-subcategories.component.sass']
})
export class CreateSubcategoriesComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faWarehouse = faWarehouse;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  parent: ParentCategory = <ParentCategory>{};
  language = localStorage.getItem('language');
  url_images = localStorage.getItem('url_images');
  public Editor = ClassicEditor;
  image:any;
  imageInit:any;
  thumbnail:any;
  id:any;
  name:any;
  description:any;
  editImage = true;
  alert:Alert = <Alert>{};


  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: CategoriesService,
              private loading: NgxSpinnerService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
        this.activatedRoute.params.subscribe( params => {
          this.id = params['parent'];
          this.parentShow(params['parent']);
      });
  }

  ngOnInit(): void {
  }

  parentShow(parent: number){
    let categories = JSON.parse(<string>localStorage.getItem('categories_admin'));
    // @ts-ignore
    this.parent = categories.find(item => item.id == parent);
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    // @ts-ignore
    let language = this.language.toString();
    let filesLength = this.files.length.toString();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('language', language);
    if (this.image){
      params.append('image', this.image);
    }
    params.append('parent_id', this.id);
    params.append('fileslength', filesLength);
    for (let f = 0; f < this.files.length; f++){
      params.append('file'+f, this.files[f]);
    }
    this.service.postCategories(params).subscribe( response => {
      let category = response;
      let categories = JSON.parse(<string>localStorage.getItem('categories_admin_all'));
      categories.push(category);
      localStorage.setItem('categories_admin_all', JSON.stringify(categories));
      this.loading.hide();
      console.log(response);
      this.router.navigateByUrl('/admin/categories');
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  closed() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closed()
    },10000);
  }

}
