import { Component, OnInit } from '@angular/core';
import { faTable, faPlus, faWarehouse } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';
import { faPowerOff, faTrashAlt,  } from '@fortawesome/free-solid-svg-icons';
import { environment } from "../../../../../environments/environment";
import { ActivatedRoute, Router } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { CategoriesService } from "../../../../services/admin/categories.service";
import {TranslationService} from "../../../../services/admin/translation.service";
import {Alert} from "../../../../interfaces/alert";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faPlus = faPlus;
  faWarehouse = faWarehouse;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  url_images = environment.baseUrl;
  alert:Alert =<Alert>{};
  categories:any[]=[];
  categoriesAll:any[]=[];
  public page: number | undefined;

  constructor(private activatedRoute: ActivatedRoute,
              private titleService: Title,
              private loading: NgxSpinnerService,
              private service: CategoriesService,
              public serviceTranslation: TranslationService,
              private router: Router) {
    this.titleService.setTitle("Lista de Productos");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.categoriesAll = JSON.parse(<string>localStorage.getItem('categories_admin_all'));
    let categories = JSON.parse(<string>localStorage.getItem('categories_admin'));
    // @ts-ignore
    this.categories = categories.filter(item => item.parent_id == null);
  }

  active(category: number){
    this.loading.show();
    this.service.activeCategories(category).subscribe( response => {
      const category = response;
      const index = this.categories.findIndex(item => item.id == category.id);
      this.categories[index].active = response.active;
      if (response.active){
        this.alert = {
          type: 'success',
          message: 'La categoria se activo con exito',
          active: true
        };
      } else {
        this.alert = {
          type: 'success',
          message: 'La categoria se desactivo con exito',
          active: true
        };
      }
      localStorage.setItem('categories_admin', JSON.stringify(this.categories));
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
  }

  delete(category: number){
    this.loading.show();
    this.service.deleteCategories(category).subscribe( response => {
      const index = this.categories.findIndex(item => item.id == response.id);
      this.categories.splice(index, 1);
      localStorage.setItem('categories_admin', JSON.stringify(this.categories));
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
    });
  }

  getParentEs(parent: number){
    let category = this.categoriesAll.filter(item => item.id == parent);
    if(category.length != 0){
      return category[0].name.es;
    } else {
      return '';
    }
  }
  getParentEn(parent: number){
    let category = this.categoriesAll.filter(item => item.id == parent);
    if(category.length != 0){
      return category[0].name.en;
    } else {
      return '';
    }
  }

  filter(search: string){
    let regex = new RegExp(search, 'i');
    if (sessionStorage.getItem('language') == 'es'){
      this.categories = this.categoriesAll.filter(item => regex.test(item.name.es));
    } else {
      this.categories = this.categoriesAll.filter(item => regex.test(item.name.en));
    }
  }

  closed() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closed()
    },10000);
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

}
