import { Component, OnInit } from '@angular/core';
import { faList, faPowerOff, faTrashAlt, faSave, faEye, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { Alert } from "../../../../interfaces/alert";
import { TranslationService } from "../../../../services/admin/translation.service";
import { NgxSpinnerService } from "ngx-spinner";
import {ActivatedRoute, Router} from "@angular/router";
import { NgForm } from "@angular/forms";
import { environment } from "../../../../../environments/environment";
import { Title} from "@angular/platform-browser";
import { ProductsService } from "../../../../services/admin/products.service";
import { Category } from "../../../../interfaces/category";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {Product} from "../../../../interfaces/product";
import {Gallery} from "../../../../interfaces/gallery";
import {Image} from "../../../../interfaces/image";
import {GalleriesService} from "../../../../services/admin/galleries.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  language = localStorage.getItem('language');
  url_images = environment.baseUrl;
  public Editor = ClassicEditor;
  alert:Alert = <Alert>{};
  gallery: Gallery = <Gallery>{};
  id:any;
  name:any;
  description:any;
  images: Image[] = [];
  image:any;
  imageInit:any;
  thumbnail:any;
  product:Product = <Product>{};
  categories: Category[]= [];
  categoriesSelect: any[]=[];
  editImage = true;
  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: ProductsService,
              private loading: NgxSpinnerService,
              private router: Router,
              private titleService: Title,
              private serviceImage: GalleriesService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['id']);
    });
  }

  ngOnInit(): void {
  }

  getData(id: number){
    let products = JSON.parse(<string>localStorage.getItem('products_admin'));
    let categories = JSON.parse(<string>localStorage.getItem('categories_admin'));
    let images = JSON.parse(<string>localStorage.getItem('images_admin'));
    let galleries = JSON.parse(<string>localStorage.getItem('galleries_admin'));
    let categorieSelect = JSON.parse(<string>localStorage.getItem('categoriesProducts_admin'));
    //@ts-ignore
    this.categoriesSelect = categorieSelect.filter(item => item.product_id == id);
    // @ts-ignore
    this.product = products.find(item => item.id == id);
    // @ts-ignore
    let galleriesCate = galleries.filter(item => item.type == 'products');
    // @ts-ignore
    this.gallery = galleriesCate.find(item => item.reference == this.category.id);
    // @ts-ignore
    this.images = images.filter(item => item.gallery_id == this.gallery.id);
    // @ts-ignore
    this.categories = categories.filter(item => item.parent_id == null);
    for(let p=0; p<this.categories.length; p++){
      for(let i=0; i<this.categoriesSelect.length; i++){
        if (this.categories[p].id == this.categoriesSelect[i].category_id){
          this.categories[p].status = true;
        }
      }
    }
    if (this.serviceTranslation.getLang() == 'fr'){
      this.titleService.setTitle("Editar " + this.product.name.fr);
    } else if (this.serviceTranslation.getLang() == 'de'){
      this.titleService.setTitle("Editar " + this.product.name.de);
    }
    this.imageInit = this.product.image;
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    // @ts-ignore
    let language = this.language.toString();
    let filesLength = this.files.length.toString();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('description', form.value.description);
    params.append('details', form.value.details);
    params.append('key', form.value.key);
    params.append('model', form.value.model);
    params.append('size', form.value.size);
    params.append('price', form.value.price);
    params.append('categories', JSON.stringify(this.categoriesSelect));
    if (this.image){
      params.append('image', this.image);
    }
    params.append('language', language);
    params.append('fileslength', filesLength);
    for (let f = 0; f < this.files.length; f++){
      params.append('file'+f, this.files[f]);
    }
    this.service.putProduct(this.product.id, params).subscribe( response => {
      let product = response;
      let products = JSON.parse(<string>localStorage.getItem('products_admin'));
      // @ts-ignore
      const index = products.findIndex(item => item.id == product.id);
      products[index] = product;
      localStorage.setItem('products_admin', JSON.stringify(products));
      this.loading.hide();
      console.log(response);
      this.router.navigateByUrl('/admin/products');
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  closed() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closed()
    },10000);
  }

  push(id: number){
    let index = this.categoriesSelect.findIndex(item => item == id);
    if (index < 0){
      this.categoriesSelect.push(id);
    } else {
      this.categoriesSelect.splice(index, 1);
    }
    console.log(this.categoriesSelect);
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  deleteImage(gallery: number){
    this.loading.show();
    this.serviceImage.deleteImage(gallery).subscribe( response => {
      // @ts-ignore
      const index = this.images.findIndex(item => item.id == response.id);
      this.images.splice(index, 1);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
    });
  }

  activeImage(gallery: number){
    this.loading.show();
    this.serviceImage.activeImage(gallery).subscribe( response => {
      const user = response;
      // @ts-ignore
      const index = this.images.findIndex(item => item.id == user.id);
      this.images[index].active = response.active;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
    });
  }

}
