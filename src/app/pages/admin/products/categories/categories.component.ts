import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {TranslationService} from "../../../../services/admin/translation.service";
import {Category} from "../../../../interfaces/category";

@Component({
  selector: 'app-product-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.sass']
})
export class CategoriesComponent implements OnInit {

  @Input() parent!: number;
  @Input() categoriesSelect:any[]=[];
  // @ts-ignore
  @ViewChild(CategoriesComponent) CategoriesComponent: CategoriesComponent;
  categories: Category[] = [];
  @Output() categorySelect: EventEmitter<number>;

  constructor(public serviceTranslation: TranslationService) {
    this.categorySelect = new EventEmitter();
  }

  ngOnInit(): void {
    this.getData();
    this.activeCategories();
  }

  getData(){
    let categories = JSON.parse(<string>localStorage.getItem('categories_admin_all'));
    // @ts-ignore
    this.categories = categories.filter(item => item.parent_id == this.parent);
  }

  activeCategories(){
    for(let p=0; p<this.categories.length; p++){
      for (let l=0; l<this.categoriesSelect.length; l++){
        if(this.categories[p].id == this.categoriesSelect[l].category_id){
          this.categories[p].status = true;
        }
      }
    }
  }

  push(id:number){
    this.categorySelect.emit(id);
  }

}
