import { Component, OnInit } from '@angular/core';
import { Alert } from "../../../../interfaces/alert";
import { Title } from "@angular/platform-browser";
import { NgxSpinnerService } from "ngx-spinner";
import { faTable, faPlus, faPowerOff, faTrashAlt, faCertificate, faStar } from '@fortawesome/free-solid-svg-icons';
import { ProductsService } from "../../../../services/admin/products.service";
import { Product } from "../../../../interfaces/product";
import { environment } from "../../../../../environments/environment";
import {TranslationService} from "../../../../services/admin/translation.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faCertificate = faCertificate;
  faStar = faStar;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  products: Product[]=[];
  public page: number | undefined;
  alert: Alert = <Alert>{};
  url_images = environment.baseUrl;

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: ProductsService,
              public serviceTranslation: TranslationService) {
    this.titleService.setTitle("Lista de Productos");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.products = JSON.parse(<string>localStorage.getItem('products_admin'));
  }

  active(id: number){
    this.loading.show();
    this.service.activeProduct(id).subscribe( response => {
      const product = response;
      const index = this.products.findIndex(item => item.id == product.id);
      this.products[index].active = response.active;
      localStorage.setItem('products_admin', JSON.stringify(this.products));
      if (response.active){
        this.alert = {
          type: 'success',
          message: 'El producto se activo correctamente',
          active: true
        };
      } else {
        this.alert = {
          type: 'success',
          message: 'El producto se desactivo correctamente',
          active: true
        };
      }
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
    this.setTime();
  }

  delete(id: number){
    this.loading.show();
    this.service.deleteProduct(id).subscribe( response => {
      const index = this.products.findIndex(item => item.id == response.id);
      this.products.splice(index, 1);
      localStorage.setItem('products_admin', JSON.stringify(this.products));
      this.alert = {
        type: 'success',
        message: 'El producto se elimino con exito',
        active: true
      };
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
    this.setTime();
  }

  closeAlert() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closeAlert()
    },10000);
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  new(id: number){
    this.loading.show();
    this.service.newProduct(id).subscribe( response => {
      const product = response;
      const index = this.products.findIndex(item => item.id == product.id);
      this.products[index].new = response.new;
      localStorage.setItem('products_admin', JSON.stringify(this.products));
      if (response.new){
        this.alert = {
          type: 'success',
          message: 'El producto es nuevo',
          active: true
        };
      } else {
        this.alert = {
          type: 'success',
          message: 'El producto dejo de ser nuevo',
          active: true
        };
      }
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
    this.setTime();
  }

  novelty(id: number){
    this.loading.show();
    this.service.noveltyProduct(id).subscribe( response => {
      const product = response;
      const index = this.products.findIndex(item => item.id == product.id);
      this.products[index].novelty = response.novelty;
      localStorage.setItem('products_admin', JSON.stringify(this.products));
      if (response.novelty){
        this.alert = {
          type: 'success',
          message: 'El producto es novedad',
          active: true
        };
      } else {
        this.alert = {
          type: 'success',
          message: 'El producto dejo de ser novedad',
          active: true
        };
      }
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
    this.setTime();
  }

}
