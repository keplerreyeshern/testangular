import { Component, OnInit } from '@angular/core';
import { faList, faWarehouse, faSave, faEye, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { Alert } from "../../../../interfaces/alert";
import { TranslationService } from "../../../../services/admin/translation.service";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { environment } from "../../../../../environments/environment";
import { Title} from "@angular/platform-browser";
import { ProductsService } from "../../../../services/admin/products.service";
import { Category } from "../../../../interfaces/category";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';



@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faWarehouse = faWarehouse;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  language = localStorage.getItem('language');
  url_images = environment.baseUrl;
  public Editor = ClassicEditor;
  alert:Alert = <Alert>{};
  image:any;
  imageInit:any;
  thumbnail:any;
  categories: Category[]= [];
  categoriesSelect: any[]=[];
  editImage = true;
  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: ProductsService,
              private loading: NgxSpinnerService,
              private router: Router,
              private titleService: Title) {
    this.titleService.setTitle("Crear Producto");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    let categories = JSON.parse(<string>localStorage.getItem('categories_admin'));
    // @ts-ignore
    this.categories = categories.filter(item => item.parent_id == null);
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    // @ts-ignore
    let language = this.language.toString();
    let filesLength = this.files.length.toString();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('description', form.value.description);
    params.append('details', form.value.details);
    params.append('key', form.value.key);
    params.append('model', form.value.model);
    params.append('size', form.value.size);
    params.append('price', form.value.price);
    params.append('categories', JSON.stringify(this.categoriesSelect));
    if (this.image){
      params.append('image', this.image);
    }
    params.append('language', language);
    params.append('fileslength', filesLength);
    for (let f = 0; f < this.files.length; f++){
      params.append('file'+f, this.files[f]);
    }
    this.service.postProduct(params).subscribe( response => {
      let product = response;
      let products = JSON.parse(<string>localStorage.getItem('products_admin'));
      product.active = true;
      product.new = true;
      product.novelty = true;
      products.push(product);
      localStorage.setItem('products_admin', JSON.stringify(products));
      this.loading.hide();
      console.log(response);
      this.router.navigateByUrl('/admin/products');
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  closed() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closed()
    },10000);
  }

  push(id: number){
    let index = this.categoriesSelect.findIndex(item => item == id);
    if (index < 0){
      this.categoriesSelect.push(id);
    } else {
      this.categoriesSelect.splice(index, 1);
    }
    console.log(this.categoriesSelect);
  }

}
