import {Component, HostListener, OnInit} from '@angular/core';
import {Meta, Title} from "@angular/platform-browser";
import {ComponentsService} from "../../services/admin/components.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.sass']
})
export class AdminComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event: Event) {
    if (window.innerWidth < 769) {
      this.serviceMenu.setClose(true);
    } else {
      this.serviceMenu.setClose(false);
    }
  }

  constructor(private title: Title,
              private meta: Meta,
              public serviceMenu: ComponentsService,) {
    this.meta.addTag({
      name: 'description',
      content: 'contenido de la descripcion'
    });
    this.title.setTitle('Administrador');
  }

  ngOnInit(): void {
  }

}
