import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { faTable, faPlus, faPowerOff, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { UsersService } from '../../../../services/admin/users.service';
import { User } from "../../../../interfaces/user";
import { Alert } from "../../../../interfaces/alert";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  users: User[]=[];
  public page: number | undefined;
  alert: Alert = <Alert>{};

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: UsersService) {
    this.titleService.setTitle("Lista de Usuarios");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.users = JSON.parse(<string>localStorage.getItem('users'));
  }

  active(user: number){
    this.loading.show();
    this.service.activeUser(user).subscribe( response => {
      const user = response;
      const index = this.users.findIndex(item => item.id == user.id);
      this.users[index].active = response.active;
      localStorage.setItem('users', JSON.stringify(this.users));
      if (response.active){
        this.alert = {
          type: 'success',
          message: 'El usuario se activo correctamente',
          active: true
        };
      } else {
        this.alert = {
          type: 'success',
          message: 'El usuario se desactivo correctamente',
          active: true
        };
      }
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
    this.setTime();
  }

  delete(user: number){
    this.loading.show();
    this.service.deleteUser(user).subscribe( response => {
      const index = this.users.findIndex(item => item.id == response.id);
      this.users.splice(index, 1);
      localStorage.setItem('users', JSON.stringify(this.users));
      this.alert = {
        type: 'success',
        message: 'El usuario se elimino con exito',
        active: true
      };
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
    this.setTime();
  }

  closeAlert() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closeAlert()
    },10000);
  }

}
