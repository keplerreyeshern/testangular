import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import { MainComponent } from './main/main.component';
import { NoticeOfPrivacyComponent } from './notice-of-privacy/notice-of-privacy.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ProductsComponent } from './products/products.component';
import {ComponentsModule} from "../../components/components.module";
import { ContactComponent } from './contact/contact.component';
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  declarations: [
    PublicComponent,
    MainComponent,
    NoticeOfPrivacyComponent,
    AboutUsComponent,
    ProductsComponent,
    ContactComponent
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    ComponentsModule,
    TranslateModule
  ]
})
export class PublicModule { }
