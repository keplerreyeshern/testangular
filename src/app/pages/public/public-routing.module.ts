import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicComponent } from './public.component';
import { MainComponent } from "./main/main.component";
import { NoticeOfPrivacyComponent } from "./notice-of-privacy/notice-of-privacy.component";
import { AboutUsComponent } from "./about-us/about-us.component";
import { ProductsComponent } from "./products/products.component";
import { ContactComponent } from "./contact/contact.component";

const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    children: [
      {
        path: '',
        component: MainComponent
      },
      {
        path: 'quienes-somos',
        component: AboutUsComponent
      },
      {
        path: 'nuestros-productos',
        component: ProductsComponent
      },
      {
        path: 'aviso-privacidad',
        component: NoticeOfPrivacyComponent
      },
      {
        path: 'contacto',
        component: ContactComponent
      },
      {
        path: '**',
        pathMatch: 'full',
        redirectTo: 'errors/404'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
