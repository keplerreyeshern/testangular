import { Component, OnInit } from '@angular/core';
import {Alert} from "../../../interfaces/alert";
import {Router} from "@angular/router";
import {AuthService} from "../../../services/auth/auth.service";
import {NgxSpinnerService} from "ngx-spinner";
import {NgForm} from "@angular/forms";
import {DataService} from "../../../services/data/data.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  storage = '';
  url = '';
  access_token = '';
  user: any;
  alert:Alert = <Alert>{};
  type = 'password';

  constructor(private spinnerService: NgxSpinnerService,
              private authService: AuthService,
              private serviceData: DataService,
              private router: Router) {}

  ngOnInit(): void {
    this.alert.type = 'danger';
  }

  submit(form: NgForm){
    this.spinnerService.show();
    const params = {
      grant_type: 'password',
      client_id: '2',
      client_secret: 'tVs301U1jAp0bC2DwrMji3v2jGiKjWexyD6tULIq',
      username: form.value.email,
      password: form.value.password,
    };
    this.authService.postToken(params).subscribe( response => {
      // console.log(response);
      localStorage.setItem('token', JSON.stringify(response));
      this.storage = <string> localStorage.getItem('token');
      let start = this.storage.indexOf('access_token', 0);
      let substr = this.storage.substring(start);
      start = substr.indexOf(':', 0) + 2;
      substr = substr.substring(start);
      const end = substr.indexOf('"', 0);
      this.access_token = substr.substring(0, end );
      localStorage.setItem('access_token', 'Bearer ' + this.access_token);
      // console.log(localStorage.getItem('access_token'));
      this.access(localStorage.getItem('access_token'));
    }, err => {
      if(err.status == 400){
        this.spinnerService.hide();
        this.alert.message = 'Las credenciales no coinciden en la base de datos. Verifica y vuelve a intentar';
        this.alert.active = true;
        this.alert.type = 'danger';
      } else if(err.status == 500){
        this.spinnerService.hide();
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
      } else if(err.status == 404){
        this.spinnerService.hide();
        this.alert.message = '¡Error Grave!, A la pagina que se intenta accesar no existe, comuniquese de inmediato con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
      } else {
        this.spinnerService.hide();
        this.alert.message = 'Se detecto un error comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
      }
      console.log(err);
      this.spinnerService.hide();
    });
  }

  access(params: any){
    this.spinnerService.show();
    this.authService.getDataUser(params).subscribe( response => {
      this.user = response;
      if (this.user.active){
        localStorage.setItem('user', JSON.stringify(this.user));
        this.authService.signIn();
        this.getDataAdmin();
      } else {
        localStorage.clear();
        this.alert.message = 'Tu usuario esta bloqueado y no tienes acceso a la aplicación';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.setTime();
        this.authService.signOut();
        this.spinnerService.hide();
      }
    }, err => {
      if(err.status == 500){
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.setTime();
        this.spinnerService.hide();
      } else if (err.status == 404) {
        this.alert.message = 'Las credenciales no conionciden con la base de datos';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.spinnerService.hide();
      } else {
        this.alert.message = 'Se detecto un error comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.setTime();
        this.spinnerService.hide();
      }
      console.log(err);
    });
  }

  setTime(){
    setTimeout(() => {
      this.alert.active = false;
    },10000);
  }

  closeAlert() {
    this.alert.active = false;
  }

  getDataAdmin(){
    this.spinnerService.show();
    this.serviceData.getDataAdmin().subscribe( response => {
      localStorage.setItem('users', JSON.stringify(response.users));
      localStorage.setItem('products_admin', JSON.stringify(response.products));
      localStorage.setItem('categoriesProducts_admin', JSON.stringify(response.categoriesProducts));
      localStorage.setItem('news_admin', JSON.stringify(response.news));
      localStorage.setItem('galleries_admin', JSON.stringify(response.galleries));
      localStorage.setItem('images_admin', JSON.stringify(response.images));
      localStorage.setItem('categories_admin', JSON.stringify(response.categories));
      localStorage.setItem('categories_admin_all', JSON.stringify(response.categories));
      localStorage.setItem('blogs_admin', JSON.stringify(response.blogs));
      this.router.navigate(['/admin']);
    }, err => {
      if (err.status == 500) {
        this.spinnerService.hide();
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.setTime();
      } else if (err.status == 404) {
        this.spinnerService.hide();
        this.alert.message = '¡Error Grave!, A la pagina que se intenta accesar no existe, comuniquese de inmediato con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.setTime();
      } else {
        this.spinnerService.hide();
        this.alert.message = 'Se detecto un error comunicate con el administrador';
        this.alert.active = true;
        this.alert.type = 'danger';
        this.setTime();
      }
      console.log(err.status);
      console.log(err);
    });
    this.spinnerService.hide();
  }

}
