import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PasswordRoutingModule } from './password-routing.module';
import { PasswordComponent } from './password.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { EmailComponent } from './email/email.component';
import { TranslateModule } from "@ngx-translate/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";
import { FindComponent } from "./find/find.component";


@NgModule({
  declarations: [
    PasswordComponent,
    EmailComponent,
    FindComponent
  ],
  imports: [
    CommonModule,
    PasswordRoutingModule,
    NgxSpinnerModule,
    TranslateModule,
    NgbModule,
    FormsModule
  ]
})
export class PasswordModule { }
